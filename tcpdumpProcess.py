#!/usr/bin/env python

import dpkt

f = open('test.pcap')
pcap = dpkt.pcap.Reader(f)

for ts, buf in pcap:
	eth = dpkt.ethernet.Ethernet(buf)
	if type(eth.data) != dpkt.ip.IP:
		print 'not ip packet.'
		continue
	ip = eth.data
	#print str(type(ip.data))
	#if type(ip.data) != dpkt.tcp.TCP:
	if ip.p!=dpkt.ip.IP_PROTO_TCP:
		print 'not tcp packet.'
		continue
	tcp = ip.data
	print 'TCP packet, using port: '+str(tcp.dport)
	if tcp.dport == 80 and len(tcp.data) > 0:
		http = dpkt.http.Request(tcp.data)
		print http.uri
f.close()